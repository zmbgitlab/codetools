# !/usr/bin/env python
# encoding: utf-8
# @Time    : 2019/1/7 13:54
# @Author  : MengboZh

from app.alipay_new.view import alipay


def alipay_configure_apis(app, api):
    app.register_blueprint(alipay, url_prefix='/alipay/')
