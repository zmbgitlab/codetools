# !/usr/bin/env python
# encoding: utf-8
# @Time    : 2019/1/7 16:34    
# @Author  : MengboZh
# sdk:python-alipay-sdk

from flask_config import ROOR_BASE_DIR
from alipay import AliPay, ISVAliPay
import os

""".pem文件相对路径"""
cacerts_key_file = ROOR_BASE_DIR + '/app/alipay_new/cert/'

"""
# ALiPay初始化对象
appid: 应用ID
app_notify_url: 默认异步回调url
app_private_key_path: 应用开发私钥
alipay_public_key_path: 支付宝的公钥
sign_type: RSA2【签名方式】
debug: 默认False,True为沙箱环境
蚂蚁沙箱支付宝网关: https://openapi.alipaydev.com/gateway.do
蚂蚁生产支付宝网关: https://openapi.alipay.com/gateway.do
"""
alipay_config = AliPay(appid="2016080300159687", app_notify_url="http://47.92.100.74:5001/alipay/notify_url",
                       app_private_key_path=os.path.join(cacerts_key_file, 'app_private_key.pem'),
                       alipay_public_key_path=os.path.join(cacerts_key_file, 'alipay_public_key.pem'),
                       sign_type="RSA2", debug=True)

"""蚂蚁沙箱(生产)支付宝网关"""
alipay_gateway = alipay_config._gateway

# 公共错误码,支付宝网关会对开发者的接口非业务调用错误做统一处理,返回码如下,
# 具体返回码详细描述可参考: https://docs.open.alipay.com/common/105806
res_code = ["1000", "20000", "20001", "40001", "40002", "40004", "40006"]


def notify_url(data):
    """
    支付异步回调通知,实际使用时配置,例如: http://47.92.100.74:5001/alipay_notify_url/
    :param data: 支付宝异步通知的数据信息
    :return:
    """
    # sign 不能参与签名验证,除去sign、sign_type两个参数外,凡是通知返回的参数皆是待验签的参数。
    signature = data.pop("sign")
    success = alipay_config.verify(data, signature)
    if success and data["trade_status"] in ("TRADE_SUCCESS", "TRADE_FINISHED"):
        return "succeed"
    elif not success and data["trade_status"] in ("WAIT_BUYER_PAY", "TRADE_CLOSED"):
        return "failed"


# ISVAliPay
isv_alipay = ISVAliPay(appid="2016080300159687", app_notify_url="http://47.92.100.74:5001/alipaynotifyurl",
                       app_private_key_path=os.path.join(cacerts_key_file, 'app_private_key.pem'),
                       alipay_public_key_path=os.path.join(cacerts_key_file, 'alipay_public_key.pem'),
                       debug=True, app_auth_token='12w13123', app_auth_code='app_auth_code')
