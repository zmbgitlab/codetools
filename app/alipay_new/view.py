# !/usr/bin/env python
# encoding: utf-8
# @Time    : 2019/1/7 13:54
# @Author  : MengboZh
import logging
import time
import random
from datetime import datetime
from copy import deepcopy
from flask import Blueprint, request, send_file, make_response, jsonify, g, redirect
from app.alipay_new.config import alipay_gateway, isv_alipay, alipay_config, res_code, notify_url
from app.common import get_random_string
from app.qrcode.utils import CreateQrCode

alipay = Blueprint('alipay', __name__)

"""sdk: python-alipay-sdk"""


@alipay.route('app_pay/', methods=['GET'])
def alipay_app_pay():
    """App支付 将order_string返回给app即可
    notify_url: 可选,不填则使用默认notify_url"""
    subject = "测试订单"
    order_string = alipay_config.api_alipay_trade_app_pay(out_trade_no=get_random_string(chars='order', length=64),
                                                          total_amount=100, subject=subject,
                                                          return_url='http://www.baidu.com')
    # app_pay_url = deepcopy(alipay_gateway + "?" + order_string)
    # return redirect(app_pay_url)
    return jsonify(order_string)


@alipay.route('page_pay/', methods=['GET'])
def alipay_page_pay():
    """
    电脑网站支付
    :param qr_pay_mode: PC扫码支付的方式, 支持前置模式和跳转模式。
    该参数默认不传, 且值为2 (订单码-跳转模式); 如果传, 值可为4 (可定义宽度的嵌入式二维码,商户可根据需要设定二维码的大小)。
    :return:
    """
    subject = "测试订单"
    order_str = alipay_config.api_alipay_trade_page_pay(out_trade_no=get_random_string(chars='order', length=64),
                                                        total_amount=100, subject=subject,
                                                        return_url='http://www.baidu.com')
    page_pay_url = deepcopy(alipay_gateway + "?" + order_str)
    return redirect(page_pay_url)


@alipay.route('wap_pay/', methods=['GET'])
def alipay_wap_pay():
    """手机网站支付"""
    subject = "测试订单"
    order_string = alipay_config.api_alipay_trade_wap_pay(out_trade_no=get_random_string(chars='order', length=64),
                                                          total_amount=100, subject=subject)
    wap_pay_url = deepcopy(alipay_gateway + "?" + order_string)
    return redirect(wap_pay_url)


@alipay.route('code_pay/', methods=['GET'])
def alipay_code_pay():
    """当面付(条码支付),目前暂用不到扫码枪设备进行当面支付。
    条码支付是支付宝给到线下传统行业的一种收款方式。商家使用扫码枪等条码识别设备扫描用户支付宝钱包上的条码/二维码，完成收款。
    用户仅需出示付款码，所有收款操作由商家端完成。
    scene(支付场景): 条码支付,取值: bar_code; 声波支付,取值: wave_code
    auth_code(用户付款码): 25~30开头的长度为16~24位的数字，实际字符串长度以开发者获取的付款码长度为准,示例值:28763443825664394
    total_amount(订单总金额): 单位为元，精确到小数点后两位,示例值:88.88
    #其他可选参数，可参考alipay开发文档
    """
    subject = "测试订单"
    result = alipay_config.api_alipay_trade_pay(out_trade_no=get_random_string(chars='order', length=64),
                                                scene="bar_code", auth_code="auth_code",
                                                subject=subject, total_amount=200)
    if result["code"] in res_code:
        return jsonify(result)


@alipay.route('precreate_pay/', methods=['GET', 'POST'])
def alipay_precreate_pay():
    """当面付(扫码支付),指用户打开支付宝钱包中的“扫一扫”功能，扫描商家展示在某收银场景下的二维码并进行支付的模式。
    该模式适用于线下实体店支付、面对面支付等场景
    qr_code_timeout_express: 该笔订单允许的最晚付款时间，逾期将关闭交易，从生成二维码开始计时。
    取值范围：1m～15d。m-分钟，h-小时，d-天，1c-当天（1c-当天的情况下，无论交易何时创建，都在0点关闭）。
    该参数数值不接受小数点， 如 1.5h，可转换为 90m。
    扫码支付: 主动轮询查询和扫码异步通知
    """
    # 接收扫码异步通知
    if request.method == 'POST':
        data = request.get_json()
        print('扫码异步通知,接收参数', data)
        return jsonify(notify_url(data))

    subject = "测试订单"
    # 实时生成订单二维码串url
    precreate_res = alipay_config.api_alipay_trade_precreate(subject=subject,
                                                             out_trade_no=get_random_string(chars='order', length=64),
                                                             total_amount=100)
    # 根据支付宝返回的二维码串值,生成二维码图片流
    make_qr_code = CreateQrCode()
    qr_code_file = make_qr_code.gen_qrcode(data=precreate_res['qr_code'])
    return qr_code_file

    # TODO:轮询，暂时未做
    # paid = False
    # for i in range(10):
    #     time.sleep(3)
    #     # 交易查询
    #     result = alipay_config.api_alipay_trade_query(out_trade_no=precreate_res['out_trade_no'])
    #     if result.get("trade_status", "") == "TRADE_SUCCESS":
    #         # 交易成功,停止轮询
    #         paid = True
    #         break
    #     print("not paid...")
    # if paid is False:
    #     # 交易失败,取消
    #     alipay_config.api_alipay_trade_cancel(out_trade_no=precreate_res['out_trade_no'])
    #     return jsonify('cancel,ok')
    # return jsonify('precreate_pay,ok')


@alipay.route('refund_pay/', methods=['POST'])
def alipay_refund_pay():
    """退款
    out_trade_no: 订单支付时传入的商户订单号,不能和 trade_no 同时为空,64位,特殊可选
    trade_no: 支付宝交易号，和商户订单号不能同时为空,64位,特殊可选
    refund_amount: 退款总金额
    refund_reason: 退款原因
    """
    data = request.get_json()
    out_trade_no = data.get('out_trade_no', '20150320010101001')
    trade_no = data.get('trade_no', '2014112611001004680073956707')
    refund_amount = data.get('refund_amount', '88.88')
    refund_reason = data.get('refund_reason', '正常退款')
    result = alipay_config.api_alipay_trade_refund(out_trade_no=out_trade_no,
                                                   trade_no=trade_no, refund_amount=refund_amount,
                                                   refund_reason=refund_reason)
    if result["code"] in res_code:
        print('msg', result["msg"])
        print('trade_no', result["trade_no"])
        print('fund_change', result["fund_change"])
        return jsonify('refund,ok')
    return jsonify('refund,failed')


@alipay.route('cancel_pay/', methods=['POST'])
def alipay_cancel_pay():
    """撤销
    out_trade_no: 原支付请求的商户订单号,和支付宝交易号不能同时为空,64位,特殊可选
    trade_no: 支付宝交易号，和商户订单号不能同时为空,64位,特殊可选
    """
    data = request.get_json()
    out_trade_no = data.get('out_trade_no', '20150320010101001')
    trade_no = data.get('trade_no', '2014112611001004680073956707')
    result = alipay_config.api_alipay_trade_cancel(out_trade_no=out_trade_no,
                                                   trade_no=trade_no)
    if result["code"] in res_code:
        print('msg', result["msg"])
        print('trade_no', result["trade_no"])
        print('action', result["action"])
        return jsonify('close,ok')
    return jsonify('close,failed')


@alipay.route('transfer_order/<string:order_id>/', methods=['GET'])
def alipay_transfer_order(order_id):
    """查询转账订单"""
    res = alipay_config.api_alipay_fund_trans_order_query(out_biz_no='20170626152216', order_id=order_id)
    return jsonify(res)


@alipay.route('fastpay_refund_query', methods=['GET'])
def alipay_order_fastpay_refund_query():
    """统一退款查询"""
    res = alipay_config.api_alipay_trade_fastpay_refund_query(out_request_no='20171120')
    return jsonify(res)


@alipay.route('only_transfer_pay', methods=['GET'])
def alipay_fund_trans_toaccount_transfer():
    """单笔转账到支付宝账户接口"""
    res = alipay_config.api_alipay_fund_trans_toaccount_transfer(out_biz_no=datetime.now().strftime("%Y%m%d%H%M%S"),
                                                                 payee_type='ALIPAY_LOGONID/ALIPAY_USERID',
                                                                 payee_account='csqnji8117@sandbox.com',
                                                                 amount='88.88')
    return jsonify(res)


@alipay.route('trade_order_settle/', methods=['GET'])
def alipay_trade_order_settle():
    """统一收单交易结算接口
    out_trade_no: 结算请求流水号 开发者自行生成并保证唯一性
    trade_no: 支付宝订单号
    royalty_parameters: 分账明细信息
    """
    data = request.get_json()
    out_trade_no = data.get('out_trade_no', '20150320010101001')
    trade_no = data.get('trade_no', '2014112611001004680073956707')
    result = alipay_config.api_alipay_trade_order_settle(out_trade_no=out_trade_no,
                                                         trade_no=trade_no, royalty_parameters=[{
            "trans_out": "2088101126765726",
            "trans_in": "2088101126708402",
            "amount": 0.1,
            "amount_percentage": 100,
            "desc": "分账给2088101126708402"
        }])
    return jsonify(result)


@alipay.route('notify_url/', methods=['POST'])
def alipay_notify_url():
    """支付异步回调通知,实际使用时配置,例如: http://47.92.100.74:5001/alipay/notify_url/
    """
    print(111, request)
    data = request.get_json()
    print("data", data)
    # data = request.form.to_dict()
    return jsonify(notify_url(data))


# ISVAliPay
@alipay.route('refresh_app_auth_token/', methods=['GET'])
def isv_alipay_open_auth_token():
    """
    刷新 refresh_token
    :return:
    """
    result = isv_alipay.api_alipay_open_auth_token_app(refresh_token='')
    return jsonify(result)


@alipay.route('auth_token_app/', methods=['GET'])
def isv_alipay_open_auth_token_app_query():
    """
    查询授权产品
    :return:
    """
    result = isv_alipay.api_alipay_open_auth_token_app_query()
    return jsonify(result)
