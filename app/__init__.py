# !/usr/bin/env python
# encoding: utf-8
# @Time    : 2019/1/7 13:54
# @Author  : MengboZh

import os
from flask import Flask
from flask_logconfig import LogConfig
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

os.environ['PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION'] = 'python'
from flask_config import config

basedir = os.path.abspath(os.path.dirname(__file__))

db = SQLAlchemy()


def create_app(config_name):
    app = Flask(__name__)

    app.config.from_object(config[config_name])
    db.init_app(app)

    api = Api(app)
    db.init_app(app)
    LogConfig(app)

    # cpp注册路由
    from app.cpp_test.url import cpp_configure_apis
    cpp_configure_apis(app, api)

    # qrcode注册路由
    from app.qrcode.url import qrcode_configure_apis
    qrcode_configure_apis(app, api)

    # alipay注册路由
    from app.alipay_new.url import alipay_configure_apis
    alipay_configure_apis(app, api)

    # wxpay注册路由
    from app.wechat_oauth2_pay.url import wechat_pay_configure_apis
    wechat_pay_configure_apis(app, api)

    return app
