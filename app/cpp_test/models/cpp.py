#!/usr/bin/python
# -*- coding: utf-8 -*-
from app import db
from sqlalchemy.dialects.postgresql import JSONB
from .mixin import utcnow


class Data(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True, nullable=True)
    name = db.Column(db.String(50), nullable=False)
    key = db.Column(db.String(50), nullable=False)
    data = db.Column(JSONB, nullable=False)

    create_time = db.Column(db.DateTime, default=lambda: utcnow())
    modify_time = db.Column(db.DateTime, onupdate=lambda: utcnow(), default=lambda: utcnow())

    is_delete = db.Column(db.Integer, nullable=True, default=0)
