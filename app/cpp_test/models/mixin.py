#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime
from app import db
from sqlalchemy.dialects.postgresql import JSONB
import iso8601


def utcnow(with_timezone=False):
    if with_timezone:
        return datetime.datetime.now(tz=iso8601.iso8601.UTC)
    return datetime.datetime.utcnow()