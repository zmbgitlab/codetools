#!/usr/bin/python
# -*- coding: utf-8 -*-
import requests
import datetime
import decimal
import json
from flask import request
from app import db
from sqlalchemy import text

# 用来判断文件是否合法
ALLOWED_EXTENSIONS = ['xls']


def get_session():
    return db.create_scoped_session(options={'autocommit': True, 'autoflush': False})


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


# 从mdm获取单个对象详细数据
def get_object_from_other(method='get', url=None, params=None, *args, **kwargs):
    """
    :param method:
    :param url:
    :param params:
    :param args:
    :param kwargs:
    :return:
    """
    Authorization = request.headers.get('Authorization')
    AuthKey = request.headers.get('AuthKey')

    if method == 'get':
        # headers = {'Authorization': request.headers.get('Authorization'),
        #            'Content-Type': 'application/json'}
        headers = {'Content-Type': 'application/json'}
        if Authorization:
            headers["Authorization"] = Authorization
        if AuthKey:
            headers["AuthKey"] = AuthKey

        response = requests.request(method=method, url=url, headers=headers)
        if response.status_code == 200:
            return response.json()
        else:
            return None
    elif method == 'post':
        response = requests.request(method=method, url=url, data=params, headers=request.headers)
        if response.status_code == 200:
            return response.json()
        else:
            return None


# rpc获取列表列表
def get_object_from_other_rpc(arg):
    Authorization = request.headers.get('Authorization')
    AuthKey = request.headers.get('AuthKey')
    obj = {"tenant_id": ""}
    if Authorization:
        obj["token"] = Authorization[7:]
    if AuthKey:
        obj["key"] = AuthKey[7:]

    data = douwa.get_message(obj, arg)
    return data


# filter data by tenant
def filter_tenant_query(db_session, cls, tenant_id):
    """
    :param db_session:  database session
    :param cls: the model you want to query
    :param tenant_id: tenant id
    :return: a query instance
    """
    return db_session.query(cls).filter_by(tenant_id=tenant_id)


# get date list
def get_date_list(start_date, end_date, date_type):
    import datetime
    from app.view.start_quantity_view import Date2Week
    start = datetime.datetime.strptime(Date2Week(start_date, date_type).date_to_date(), '%Y-%m-%d')
    print(start)
    end = datetime.datetime.strptime(Date2Week(end_date, date_type).date_to_date(), '%Y-%m-%d')
    current = start
    date_list = []
    # 如果是以周为时间维度
    if date_type == 'week':

        while current <= end:
            date_list.append(current.strftime('%Y-%m-%d'))
            current = current + datetime.timedelta(weeks=1)
    # 如果是以月为时间维度
    elif date_type == 'month':
        while current <= end:
            date_list.append(current.strftime('%Y-%m-%d'))
            if current.month == 12:
                year = current.year + 1
                next_month = 1
            else:
                year = current.year
                next_month = current.month + 1
            current = datetime.datetime(year=year, month=next_month, day=current.day)
    return date_list


'''
def build_distribution_cte(distribution_by, minv, maxv):
    """
    created by Hellwen.wu at 2017-12-04
    paramters:
        distribution_by: 200,300,400,500
    通过一个数字列表返回sqlalchemy的cte，用于数据统计的分布处理
    这里并没有考虑数据非法判断，请后续自行添加
    """
    dis_list = distribution_by.split(',')
    rows = []
    last = 0
    for idx, dis in enumerate(dis_list):
        if idx == 0:
            rows.append((minv, dis, '<' + str(dis)))
            last = dis
        elif idx < (len(dis_list) - 1):
            rows.append((last, dis, '<' + str(dis)))
            last = dis
        elif idx == (len(dis_list) - 1):
            rows.append((last, dis, '<' + str(dis)))
            rows.append((dis, maxv, '>=' + str(dis)))

    stmts = [
        sa.select([
            sa.cast(sa.literal(i), sa.Integer).label("start"),
            sa.cast(sa.literal(v), sa.Integer).label("end"),
            sa.cast(sa.literal(d), sa.String).label("range"),
        ]) if idx == 0 else
        sa.select([sa.literal(i), sa.literal(v), sa.literal(d)])  # no type cast
        for idx, (i, v, d) in enumerate(rows)
        ]
    query = sa.union_all(*stmts)
    return query.cte(name="distribution_cte")


@report_quantity.route("testcte")
def testcte():
    distribution_by = request.args.get('distribution_by')
    distribution_cte = build_distribution_cte(distribution_by, -1000000, 1000000)
    db_session = get_session()
    return jsonify([res for res in db_session.query(distribution_cte)])
'''


def alchemyencoder(obj):
    """JSON encoder function for SQLAlchemy special classes."""
    if isinstance(obj, datetime.date):
        return obj.isoformat()
    elif isinstance(obj, decimal.Decimal):
        return float(obj)


def sql_add_date_aggregate(date_type, sql, date_field="aggregate_date", quantity_field="quantity"):
    new_sql = ''
    if date_type == 'month':
        new_sql = """
select extract('year' from a.{date_field}) || '-' || to_char(extract('month' from a.{date_field}), 'FM00') as aggregate_date
    , sum(a.{quantity_field}) as quantity
from ({sql}) a
group by extract('year' from a.{date_field}), extract('month' from a.{date_field})
order by extract('year' from a.{date_field}), extract('month' from a.{date_field})
""".format(sql=sql, date_field=date_field, quantity_field=quantity_field)
    elif date_type == 'week':
        new_sql = """
select extract('year' from a.{date_field}) || '-' || to_char(extract('week' from a.{date_field}), 'FM00') as aggregate_date
    , sum(a.{quantity_field}) as quantity
from ({sql}) a
group by extract('year' from a.{date_field}), extract('week' from a.{date_field})
order by extract('year' from a.{date_field}), extract('week' from a.{date_field})
""".format(sql=sql, date_field=date_field, quantity_field=quantity_field)

    return new_sql


def get_sql_result(sql, params):
    db_session = get_session()
    s = text(sql)
    # print('exce sql text:', s)
    result = db_session.execute(s, params)
    # logger.debug('get data: %s' % dataset)
    return result


def result2json(result):
    return json.loads(json.dumps([dict(r) for r in result], default=alchemyencoder))


def sql2json(sql, params):
    return result2json(get_sql_result(sql, params))
