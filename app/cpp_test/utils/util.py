# -*- coding:utf8 -*-

from flask import g
from sqlalchemy import or_


# def filter_group_query(cls):
#     if g.user['datapermission']['type'] == 'self':
#         return cls.query.filter(cls.creator_ugid == g.user['group']['id'])
#     elif g.user['datapermission']['type'] == 'group':
#         return cls.query.filter(cls.creator_ugid.in_(g.user['datapermission']['group']))
#     elif g.user['datapermission']['type'] == 'all':
#         return cls.query

def filter_group_query(db_session, cls):
    """
    :param db_session:  database session
    :param cls: the model you want to query
    :return: a query instance
    """
    query = db_session.query(cls)
    if g.user['datapermission']['type'] == 'self':
        return query.filter(cls.creator_ugid == g.user['group']['id'])
    elif g.user['datapermission']['type'] == 'all':
        return query
    elif g.user['datapermission']['type'] == 'group':
        return query.filter(cls.creator_ugid.in_(g.user['datapermission']['groups']))

def filter_group_query_many_field(db_session, cls, *args, **kwargs):
    """
    :param db_session:  database session
    :param cls: the model you want to query
    :return: a query instance
    """
    query = db_session.query(*args, **kwargs)
    if g.user['datapermission']['type'] == 'self':
        return query.filter(cls.creator_ugid == g.user['group']['id'])
    elif g.user['datapermission']['type'] == 'all':
        return query
    elif g.user['datapermission']['type'] == 'group':
        return query.filter(cls.creator_ugid.in_(g.user['datapermission']['groups']))

def filter_group_query_sql(g):
    if g.user['datapermission']['type'] == 'self':
        sql_format = {
            "str": '''and creator_ugid = :group''',
            "data": g.user['group']['id']
        }
    elif g.user['datapermission']['type'] == 'group':
        # x = "and creator_ugid in (g.user['datapermission']['group'])"
        sql_format = {
            "str": '''and creator_ugid in :group''',
            "data": tuple(g.user['datapermission']['groups'])
        }
    elif g.user['datapermission']['type'] == 'all':
        sql_format = {
            "str": "",
            "data": None
        }
    return sql_format


def filter_to_query_record(db_session, cls, *args, **kwargs):

    query = db_session.query(*args, **kwargs)

    query = query.filter(cls.to == g.user["group"].get("code"))
                                 # cls.to == None
                                # cls.to == ""))

    return query


def pagination(data, page, per_page):
    # data = self.get_queryset(self.db_session)

    # page = request.args.get('page')
    page = int(1 if page is None else page)
    # per_page = request.args.get('per_page')
    per_page = int(10 if per_page is None else per_page)

    data = data.paginate(page=page, per_page=per_page, error_out=False)

    # result = DataSchema().dump(data.items, many=True).data
    return data
