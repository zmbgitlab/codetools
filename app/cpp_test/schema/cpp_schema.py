from marshmallow import Schema, fields


class CppSchema(Schema):
    id = fields.String()
    name = fields.String(required=True)
    key = fields.String(required=True)
    data = fields.String(required=True)
    create_time = fields.DateTime(format='%Y-%m-%d %H:%M:%S')
    modify_time = fields.DateTime(format='%Y-%m-%d %H:%M:%S')
    is_delete = fields.Integer(allow_none=True)
