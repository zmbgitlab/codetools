#!/usr/bin/python
# -*- coding: utf-8 -*-

from app.cpp_test.views.pro_view import CppListView
from app.cpp_test.views.cpp import CppView


def cpp_configure_apis(app, api):
    # 查询数据-(get)
    api.add_resource(CppListView, '/detector_count/')
    # 上传数据接口-(post)
    api.add_resource(CppView, '/gate/')
