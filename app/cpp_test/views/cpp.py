#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import request
from flask_restful import Resource
import logging
from app.cpp_test.utils import get_session
from app.cpp_test.models.cpp import Data

logger = logging.getLogger()


class CppView(Resource):

    def __init__(self):
        self.db_session = get_session()

    def post(self):
        data = request.get_json()
        name = data.get('name')
        key = data.get('key')
        hc_data = data.get('data')
        to = data.get('to')
        data_exist = self.db_session.query(Data).filter(Data.name == name, Data.key == key).first()
        try:
            if not data_exist:
                if data['type'] == 'add':
                    logger.debug('adding data')
                    data_ = Data()
                    data_.name = name
                    data_.key = key
                    data_.data = hc_data
                    data_.to = to
                    self.db_session.add(data_)
                    self.db_session.flush()
                else:
                    logger.error("type类型错误")
                    return {"errormsg": "type类型错误"}
            else:
                if data['type'] == 'add':
                    logger.debug('updating data')
                    data_exist.name = name
                    data_exist.key = key
                    data_exist.data = hc_data
                    data_exist.to = to
                    self.db_session.add(data_exist)
                elif data['type'] == 'del':
                    logger.debug('delete data')
                    self.db_session.delete(data_exist)
                else:
                    logger.error("type类型错误")
                    return {"errormsg": "type类型错误"}
                self.db_session.flush()
        except Exception as e:
            logger.error(e)
            self.db_session.rollback()
            return False
        return True
