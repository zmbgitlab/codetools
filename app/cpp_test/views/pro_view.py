#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask_restful import Resource
from app.cpp_test.utils.common import get_session
from app.cpp_test.models.cpp import Data
from app.cpp_test.schema.cpp_schema import CppSchema


class CppListView(Resource):
    '''查询cpp数据'''

    def get(self):
        db_session = get_session()
        aaaa = db_session.query(Data).filter_by(is_delete=0).all()
        data, error = CppSchema(many=True).dump(aaaa)
        if error:
            return error, 400
        return data
