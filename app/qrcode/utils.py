# !/usr/bin/env python
# encoding: utf-8
# @Time    : 2019/1/7 13:54
# @Author  : MengboZh
import sys
import datetime
import hashlib
import time
import qrcode
import os
from PIL import Image
from flask import send_file, make_response

try:
    # python2
    from cStringIO import StringIO
except ImportError:
    # python3 +
    from io import StringIO, BytesIO


class CreateQrCode(object):
    def __init__(self, version=4,
                 error_correction=qrcode.constants.ERROR_CORRECT_Q, box_size=8, border=4):
        """
        :param version: 生成二维码尺寸的大小 1-40  1:21*21（21+(n-1)*4）
        :param error_correction: 报错级别
        :param box_size: 每个格子的像素大小
        :param border: 边框的格子宽度大小
        """
        # 数据值，默认为百度首页
        self.data = "www.baidu.com"
        # 以下为qrcode内部参数
        self.version = version
        self.error_correction = error_correction
        self.box_size = box_size
        self.border = border

    def version_resonse(self):
        """
        # 获取本地Python语言版本,做其他操作
        :return:
        """
        if sys.version[:3] == '3.5':
            pass
        elif sys.version[:3] == '2.7':
            pass

    def qr_file_name(self):
        """
        # 生成二维码图片名称
        :return:
        """
        timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S_')
        md5_string = hashlib.md5(self.data.encode('utf8')).hexdigest()
        return timestamp + md5_string + '.png'

    def gen_qrcode(self, data=None):
        """
        # 生成二维码,以图片流形式返回
        :return:
        """
        try:
            data = data if data else self.data
            img = qrcode.make(data)
            # buf = StringIO() # python2, django1.9.4
            buf = BytesIO()
            img.save(buf)

            image_stream = buf.getvalue()
            """
            ## python2, django1.9.4,浏览器会缓存图片，提高再次加载的速度
            response = HttpResponse(image_stream, content_type="image/png")
            last_modified = time.strftime("%a,%d %b %Y %H:%M:%S GMT", time.gmtime())
            response['Last-Modified'] = last_modified
            response['Cache-Control'] = 'max-age=31536000'
            """
            response = make_response(image_stream)
            response.headers['Content-Type'] = 'image/png'
            return response
        except Exception as e:
            RuntimeError(e)

    def gen_color_log_qrcode(self, data=None, logo_url=None, image_file=None):
        """
        # 生成带logo的彩色二维码图片保存本地,并以图片流形式返回
        :param data: 数据值，默认为百度首页
        :param logo_url: logo的相对路径
        :param image_file: 保存二维码图片的相对路径
        :return:
        """

        try:
            data = data if data else self.data
            img = qrcode.make(data=data, version=self.version, error_correction=self.error_correction,
                              box_size=self.box_size, border=self.border)
            img = img.convert("RGBA")
            # 添加logo
            icon = Image.open(logo_url)
            # 获取二维码图片的大小
            img_w, img_h = img.size
            factor = 4
            size_w = int(img_w / factor)
            size_h = int(img_h / factor)
            # logo图片的大小不能超过二维码图片的1/4
            icon_w, icon_h = icon.size
            if icon_w > size_w:
                icon_w = size_w
            if icon_h > size_h:
                icon_h = size_h
            icon = icon.resize((icon_w, icon_h), Image.ANTIALIAS)
            # 计算logo在二维码图中的位置
            w = int((img_w - icon_w) / 2)
            h = int((img_h - icon_h) / 2)
            icon = icon.convert("RGBA")
            img.paste(icon, (w, h), icon)

            img_name = self.qr_file_name()
            img.save(image_file + img_name)
            img_url = os.path.join(image_file, img_name)
            file = send_file(img_url)
            response = make_response(file)
            response.headers['Content-Type'] = 'image/png'
            # 删除本地保存的彩色二维码图片,删不删由你决定
            file_path = os.path.exists(img_url)
            if file_path is True:
                os.remove(img_url)
            return response
        except Exception as e:
            RuntimeError(e)
