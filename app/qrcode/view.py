# !/usr/bin/env python
# encoding: utf-8
# @Time    : 2019/1/7 13:54
# @Author  : MengboZh
import logging
from flask import Blueprint, request, send_file, make_response, jsonify, g
from flask_config import EXCEL_PATH
from app.qrcode.utils import CreateQrCode

image_file = EXCEL_PATH

logger = logging.getLogger()

"""蓝图注册方法"""
qr_code = Blueprint('qr_code', __name__)


@qr_code.route('gen_qr_code/', methods=['GET'])
def generate_qrcode():
    """生成二维码,以图片流形式返回"""
    make_qrcode = CreateQrCode()
    return make_qrcode.gen_qrcode()


@qr_code.route('gen_color_logo_qr_code/', methods=['GET'])
def make_logo_qr_img():
    """生成带logo的彩色二维码图片保存本地,并以图片流形式返回"""
    logo_url = image_file + '/bd_logo.png'
    make_color_log_qrcode = CreateQrCode()
    return make_color_log_qrcode.gen_color_log_qrcode(logo_url=logo_url, image_file=image_file + '/images/')
