# !/usr/bin/env python
# encoding: utf-8
# @Time    : 2019/1/7 13:54
# @Author  : MengboZh


from app.qrcode.view import qr_code


def qrcode_configure_apis(app, api):
    app.register_blueprint(qr_code, url_prefix='/qr_code/')
