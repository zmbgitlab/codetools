# !/usr/bin/env python
# encoding: utf-8
# @Time    : 2019/1/7 13:54
# @Author  : MengboZh

from app.wechat_oauth2_pay.oauth2_view import wechat_oauth2
from app.wechat_oauth2_pay.pay_view import wechat_pay


def wechat_pay_configure_apis(app, api):
    app.register_blueprint(wechat_oauth2, url_prefix='/wxoauth2/')
    app.register_blueprint(wechat_pay, url_prefix='/wxpay/')
    app.register_blueprint(wechat_oauth2, url_prefix='/')
