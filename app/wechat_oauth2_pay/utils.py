# !/usr/bin/env python
# encoding: utf-8
# @Time    : 2019/1/10 14:49
# @Author  : MengboZh
import string
import random
import time
import json
import requests
from xml.dom import minidom
import xml.etree.ElementTree as ET
import hashlib
import hmac
import six
from urllib.request import urlopen
from urllib import parse
from urllib3 import request
from copy import deepcopy


class WeChatOauth2(object):
    @property
    def appid(self):
        return self._appid

    @property
    def key(self):
        return self._key

    @property
    def mch_id(self):
        return self._mch_id

    @property
    def sub_appid(self):
        return self._sub_appid

    @property
    def sub_mch_id(self):
        return self._sub_mch_id

    @property
    def timeStamp(self):
        return str(int(time.time()))

    @property
    def nonce_str(self, length=32):
        return ''.join(random.sample(string.ascii_letters + string.digits, length))

    @property
    def sign_type(self):
        return self._sign_type

    @property
    def notify_url(self):
        return self._notify_url

    @property
    def app_sslcert(self):
        return self._sslcert_path

    @property
    def app_sslcert_key(self):
        return self._sslcert_key_path

    def __init__(self, appid, mch_id, key, sslcert_path, sslcert_key_path, notify_url=None, sub_appid=None,
                 sub_mch_id=None,
                 curl_timeout=None, sandbox=False, sign_type="MD5"):
        """
        :param appid: 必填,受理商户的appid 或 微信公众号的appid,两者不同
        :param key: 必填,商户支付密钥Key,审核通过后,在微信发送的邮件中查看
        :param mchid: 必填,商户号
        :param sslcert_path: 必填,商户证书相对路径,
        :param sslkey_path: 必填,商户证书私钥相对路径
        :param sub_appid: 可选,小程序appid
        :param sub_mch_id: 可选,子商户号,受理模式下必填
        :param notify_url: 可选,异步回调url,商户根据实际开发过程设定
        :param curl_timeout: 可选,请求超时时间,单位秒,默认无超时设置
        :param sandbox: 可选,是否使用测试环境,默认为 False
        :param sign_type: 可选,签名类型,目前支持HMAC-SHA256和MD5,默认为MD5
        """
        self._appid = appid
        self._key = key
        self._mch_id = mch_id
        self._sslcert_path = sslcert_path
        self._sslcert_key_path = sslcert_key_path
        self._sub_appid = sub_appid
        self._sub_mch_id = sub_mch_id
        # 以下参数可选
        self._notify_url = notify_url
        self._curl_timeout = curl_timeout
        self._sandbox = sandbox
        self._sign_type = sign_type
        # 生产环境url
        self._gateway = "https://api.mch.weixin.qq.com"
        # 用户同意App授权,获取code的url
        self.code_redirect_url = "https://open.weixin.qq.com/connect/oauth2/authorize?%s#wechat_redirect"
        # 用户同意PC扫码授权,获取code的url
        self.qrcode_redirect_url = "https://open.weixin.qq.com/connect/qrconnect?%s#wechat_redirect"
        # 通过code换取网页授权access_token、openid的url
        self.openid_url = "https://api.weixin.qq.com/sns/oauth2/access_token?%s&grant_type=authorization_code"
        # 刷新access_token的url(如果需要)
        self.refresh_access_token_url = "https://api.weixin.qq.com/sns/oauth2/refresh_token?%s"
        # 拉取用户信息的url
        self.userinfo_url = "https://api.weixin.qq.com/sns/userinfo?%s&lang=zh_CN"
        # 拉取用户基本信息的url
        self.userinfo_all_url = "https://api.weixin.qq.com/cgi-bin/user/info?%s&lang=zh_CN"
        # 检验授权凭证的url
        self.check_access_token_url = "https://api.weixin.qq.com/sns/auth?%s"
        # 校验access_token是否有效的url
        self.vertify_access_token_url = "https://api.weixin.qq.com/sns/auth?%s"
        self._http = requests.Session()

    # 以下是网站(移动)应用微信登录开发步骤
    def gen_code(self, data):
        """
        # 第一步:App, jsapi授权登录获取,请求code
        :param appid: (必填)应用唯一标识
        :param redirect_uri: (必填)请使用urlEncode对链接进行处理
        :param response_type: (必填)填code,response_type=code
        :param scope: (必填)应用授权作用域,拥有多个作用域用逗号（,）分隔,网页应用目前仅填写 scope=snsapi_login 即可
        :param state: (可选)用于保持请求和回调的状态，授权请求后原样带回给第三方。该参数可用于防止csrf攻击（跨站请求伪造攻击）,
        建议第三方带上该参数，可设置为简单的随机数加session进行校验
        :return:
        """
        params_dict = {
            'appid': self.appid,
            'redirect_uri': data['redirect_uri'],
            'response_type': 'code',
            'scope': 'snsapi_userinfo',
            'state': data['state'],
        }
        # foo_url = parse.urlencode(params_dict).encode('utf-8')
        foo_url = self.params_urlencode(params_dict)
        url = deepcopy(self.code_redirect_url % foo_url)
        return url

    def gen_qr_code(self, data):
        """
        # 扫码授权登录获取,请求code
        :return:
        """
        params_dict = {
            'appid': self.appid,
            'redirect_uri': data['redirect_uri'],
            'response_type': 'code',
            'scope': 'snsapi_login',
            'state': data['state'],
        }
        # foo_url = parse.urlencode(params_dict).encode('utf-8')
        foo_url = self.params_urlencode(params_dict)
        url = deepcopy(self.qrcode_redirect_url % foo_url)
        return url

    def gen_code2access_token(self, code):
        """
        # 第二步:通过code获取access_token, openid
        :return:
        """
        params_dict = {'appid': self.appid, 'secret': self.app_sslcert, 'code': code}
        foo_url = self.params_urlencode(params_dict)
        url = deepcopy(self.openid_url % foo_url)
        res = self._get_data(url=url)
        return json.loads(res.text)

    def gen_refresh_token(self, refresh_token):
        """
        # 刷新access_token有效期
        :param refresh_token: 用户刷新access_token
        :return:
        """
        params_dict = {
            'appid': self.appid,
            'grant_type': 'refresh_token',
            'refresh_token': refresh_token
        }
        foo_url = self.params_urlencode(params_dict)
        url = deepcopy(self.refresh_access_token_url % foo_url)
        res = self._get_data(url=url)
        return json.loads(res.text)

    def gen_access_token_snsapi(self, openid):
        """
        # 第三步:通过access_token调用接口
        :return:
        """
        params_dict = {
            'appid': self.appid,
            'openid': openid
        }
        foo_url = self.params_urlencode(params_dict)
        url = deepcopy(self.userinfo_url % foo_url)
        res = self._get_data(url=url)
        return json.loads(res.text)

    def check_access_token(self, data):
        """
        # 检验授权凭证（access_token）是否有效"
        :return:
        """
        params_dict = {
            'access_token': data['access_token'],
            'openid': data['openid']
        }
        foo_url = self.params_urlencode(params_dict)
        url = deepcopy(self.check_access_token_url % foo_url)
        res = self._get_data(url=url)
        return json.loads(res.text)

    def get_userinfo(self, data):
        """
        # 拉取用户基本信息
        :param data:
        :return:
        """
        params_dict = {
            'access_token': data['access_token'],
            'openid': data['openid']
        }
        foo_url = self.params_urlencode(params_dict)
        url = deepcopy(self.userinfo_all_url % foo_url)
        res = self._get_data(url=url)
        return json.loads(res.text)

    def _post_data(self, url, data, cert=None):
        res = requests.post(url, data, cert=cert)
        res.encoding = 'UTF-8'
        return res.text.encode('utf-8')

    def _get_data(self, url, cert=None):
        res = requests.get(url, cert=cert)
        res.encoding = 'UTF-8'
        return res.text.encode('utf-8')

    def params_urlencode(self, params_dict):
        un_string = "&".join("{}={}".format(k, v) for k, v in params_dict)
        return parse.urlencode(un_string).encode('utf-8')


class WeChatPay(WeChatOauth2):

    def _fetch_sandbox_api_key(self):
        """
        # 获取沙箱API密钥
        :return:
        """

        sign = self._sign_data({'mch_id': self.mch_id, 'nonce_str': self.nonce_str})
        payload = self._dict2xml({'mch_id': self.mch_id, 'nonce_str': self.nonce_str, 'sign': sign})
        api_url = '{0}sandboxnew/pay/getsignkey'.format(self._gateway)
        response = self._http.post(api_url, data=payload, headers={'Content-Type': 'text/xml'})
        return self._xml2dict(response.text)['xml'].get('sandbox_signkey')

    @property
    def sandbox_api_key(self):
        if self._sandbox:
            return self._fetch_sandbox_api_key()

    def _sign_key(self, unsigned_string):
        """
        # 拼接API密钥,key为商户平台设置的密钥key
        :param unsigned_string:
        :return:
        """
        signed_string = "{}&key={}".format(unsigned_string, self.key)
        return signed_string

    def _sign_data(self, data):
        """
        # 计算签名
        :param data: 签名数据
        :return:
        """
        # 排序
        ordered_items = list(sorted([(k, v) for k, v in data.items()]))
        # 拼接成URL键值对的格式(即key1=value1&key2=value)
        unsigned_string = "&".join("{}={}".format(k, v) for k, v in ordered_items)
        # 拼接key
        signed_string = self._sign_key(unsigned_string)
        if self.sign_type == 'MD5':
            # MD5运算
            sign = hashlib.md5(signed_string.encode())
        else:  # SHA256
            sign = hmac.new(self.key.encode(), msg=signed_string.encode(), digestmod=hashlib.sha256)
        # 转为十六进制字符串,并将字符串转换为大写
        data["sign"] = sign.hexdigest().upper()
        return data

    def _verify_sign(self, data):
        """
        # 验签
        :param data: 验签数据
        :return:
        """

        notify_sign = data['sign']
        temp_data = deepcopy(data)
        temp_data.pop("sign")
        local_sign = self._sign_data(temp_data)
        if notify_sign == local_sign:
            return True
        return False

    def _build_unified_body(self, **kwargs):
        """
        构建统一下单数据
        """
        data = {
            "appid": self.appid,
            "mch_id": self.mch_id,
            "nonce_str": self.nonce_str,
            "sign_type": self.sign_type
        }
        data.update(**kwargs)
        return data

    def build_json_data(self, data):
        """
        构建统一下单接口返回的json数据
        :param data:
        :return:
        """
        if data["return_code"] == "SUCCESS":
            if data['result_code'] == "SUCCESS" and data["trade_type"] == "NATIVE":
                res = {"code_url": data['code_url']}
            elif data['result_code'] == "SUCCESS" and data["trade_type"] == "APP":
                res = {"appId": self.appid,
                       "prepay_id": data['prepay_id'],
                       "nonceStr": self.nonce_str,
                       "timeStamp": self.timeStamp,
                       "package": "Sign=WXPay"}
                res["sign"] = self._sign_data(res)['sign']
            elif data['result_code'] == "SUCCESS" and data["trade_type"] == "JSAPI":
                res = {
                    "appId": self.appid,
                    "nonceStr": self.nonce_str,
                    "package": "prepay_id%s" % data["prepay_id"],
                    "signType": self.sign_type,
                    "timeStamp": self.timeStamp,
                }
                res["paySign"] = self._sign_data(res)
            elif data['result_code'] == "SUCCESS" and data["trade_type"] == "MWEB":
                res = {"mweb_url": data['mweb_url']}
            else:
                res = {"result_code": data['result_code']}
            return res
        return data

    def _xml2dict(self, data):
        """
        :param data: xml
        :return: dict
        """
        return dict((child.tag, child.text) for child in ET.fromstring(data))
        # res = {}
        # elements = ET.fromstring(data)
        # for element in elements.iter():
        #     if element.tag == "xml":
        #         continue
        #     res[element.tag] = element.text
        # return res

    def _dict2xml(self, data):
        """
        :param data: dict
        :return: xml
        """
        if not isinstance(data, dict):
            raise TypeError('data object must be a dict type')
        dom = minidom.Document()
        root_code = dom.createElement("xml")
        dom.appendChild(root_code)
        for k, v in data.items():
            sub_node = dom.createElement(k)
            root_code.appendChild(sub_node)
            text = dom.createTextNode(str(v))
            sub_node.appendChild(text)
        return dom.toprettyxml(encoding='utf-8')

    def unified_order(self, **kwargs):
        """
        # 统一下单
        :param out_trade_no:(必填),商户系统内部订单号,要求32个字符内,只能是数字、大小写字母_-|*且在同一个商户号下唯一。
        :param body:(必填),商品描述
        :param total_fee:(必填),订单总金额,只能为整数
        :param trade_type: (必填),交易类型,单位为【分】
        :param notify_url: (可填),回调地址
        :return: return_code(返回状态码),return_msg(返回信息)
        """
        # 统一下单请求地址
        url = self._gateway + '/pay/unifiedorder'
        # 构建统一下单数据
        data = self._build_unified_body(**kwargs)
        # 签名
        param = self._sign_data(data)
        # 字典转XML
        xml_data = self._dict2xml(param)
        # 发送xml格式的统一下单请求
        raw_string = self._post_data(url, xml_data)
        # 处理微信返回的xml数据转字典
        return self._xml2dict(raw_string)

    def query_order(self, **kwargs):
        """
        # 查询订单
        :param transaction_id:(必填),微信订单号(优先使用)和商户订单号,二选一。
        :param out_trade_no:(必填),商户订单号
        :return: return_code(返回状态码),return_msg(返回信息)
        """
        # 查询订单请求地址
        url = self._gateway + '/pay/orderquery'
        # 构建统一下单数据
        data = self._build_unified_body(**kwargs)
        # 签名
        param = self._sign_data(data)
        # 字典转XML
        xml_data = self._dict2xml(param)
        # 发送xml格式的统一下单请求
        raw_string = self._post_data(url, xml_data)
        # 处理微信返回的xml数据转字典
        return self._xml2dict(raw_string)

    def close_order(self, **kwargs):
        """
        # 关闭订单
        :param out_trade_no:(必填),商户订单号
        :return: return_code(返回状态码),return_msg(返回信息)
        """
        # 关闭订单请求地址
        url = self._gateway + '/pay/closeorder'
        # 构建统一下单数据
        data = self._build_unified_body(**kwargs)
        # 签名
        param = self._sign_data(data)
        # 字典转XML
        xml_data = self._dict2xml(param)
        # 发送xml格式的统一下单请求
        raw_string = self._post_data(url, xml_data)
        # 处理微信返回的xml数据转字典
        return self._xml2dict(raw_string)

    def refund(self, **kwargs):
        """
        # 申请退款
        :param transaction_id:(必填),微信订单号(优先使用)和商户订单号,二选一。
        :param out_trade_no:(必填),商户订单号
        :param total_fee:(必填),订单金额
        :param refund_fee:(必填),申请退款金额
        :return: return_code(返回状态码),return_msg(返回信息)
        """
        # 申请退款请求地址
        url = self._gateway + '/secapi/pay/refund'
        # 构建统一下单数据
        data = self._build_unified_body(**kwargs)
        # 签名
        param = self._sign_data(data)
        # 字典转XML
        xml_data = self._dict2xml(param)
        # 发送xml格式的统一下单请求
        raw_string = self._post_data(url, xml_data, cert=(self.app_sslcert, self.app_sslcert_key))
        # 处理微信返回的xml数据转字典
        return self._xml2dict(raw_string)

    def refund_query(self, **kwargs):
        """
        # 查询退款
        四选一、查询的优先级是： refund_id > out_refund_no > transaction_id > out_trade_no
        :param refund_id:(必填),微信退款单号
        :param out_trade_no:(必填),商户订单号
        :param transaction_id:(必填),微信订单号
        :param out_refund_no:(必填),商户退款单号
        :return: return_code(返回状态码),return_msg(返回信息)
        """
        # 查询退款请求地址
        url = self._gateway + '/pay/refundquery'
        # 构建统一下单数据
        data = self._build_unified_body(**kwargs)
        # 签名
        param = self._sign_data(data)
        # 字典转XML
        xml_data = self._dict2xml(param)
        # 发送xml格式的统一下单请求
        raw_string = self._post_data(url, xml_data, cert=(self.app_sslcert, self.app_sslcert_key))
        # 处理微信返回的xml数据转字典
        return self._xml2dict(raw_string)

    def download_bill(self, **kwargs):
        """
        # 下载对账单
        :param bill_date:(必填),对账单日期
        :return: return_code(返回状态码),return_msg(返回信息)
        """
        # 下载对账单请求地址
        url = self._gateway + '/pay/downloadbill'
        # 构建统一下单数据
        data = self._build_unified_body(**kwargs)
        # 签名
        param = self._sign_data(data)
        # 字典转XML
        xml_data = self._dict2xml(param)
        # 发送xml格式的统一下单请求
        raw_string = self._post_data(url, xml_data)
        # 处理微信返回的xml数据转字典
        return self._xml2dict(raw_string)

    def report(self, **kwargs):
        """
        # 交易保障
        :param interface_url:(必填).接口URL
        :param execute_time:(必填),接口耗时
        :param return_code:(必填),返回状态码
        :param result_code:(必填),业务结果
        :param user_ip:(必填),访问接口IP
        :return: return_code(返回状态码),return_msg(返回信息)
        """
        # 交易保障请求地址
        url = self._gateway + '/pay/downloadfundflow'
        # 构建统一下单数据
        data = self._build_unified_body(**kwargs)
        # 签名
        param = self._sign_data(data)
        # 字典转XML
        xml_data = self._dict2xml(param)
        # 发送xml格式的统一下单请求
        raw_string = self._post_data(url, xml_data, cert=(self.app_sslcert, self.app_sslcert_key))
        # 处理微信返回的xml数据转字典
        return self._xml2dict(raw_string)

    def long_url2short_url(self, **kwargs):
        """ TODO
        # 转换短链接(该接口主要用于Native支付模式一中的二维码链接转成短链接,减小二维码数据量,提升扫描速度和精确度)
        :param long_url:(必填),需要转换的URL,签名用原串,传输需对URL进行encode
        :return: return_code(返回状态码),return_msg(返回信息)
        """
        # 转换短链接请求地址
        url = self._gateway + '/tools/shorturl'
        return

    def micro_pay(self, **kwargs):
        """
        # 付款码支付
        :param out_trade_no:(必填),商户系统内部订单号,要求32个字符内,只能是数字、大小写字母_-|*且在同一个商户号下唯一。
        :param body:(必填),商品描述
        :param total_fee:(必填),订单总金额,只能为整数
        :param trade_type: (必填),交易类型
        :return: return_code(返回状态码),return_msg(返回信息)
        """
        # 付款码请求地址
        url = self._gateway + '/pay/micropay'
        # 构建付款码下单数据
        data = {"appid": self.appid, "mch_id": self.mch_id, "nonce_str": self.nonce_str, "sign_type": self.sign_type,
                "spbill_create_ip": "127.0.0.1", }
        data.update(**kwargs)
        # 签名
        param = self._sign_data(data)
        # 字典转XML
        xml_data = self._dict2xml(param)
        # 发送xml格式的统一下单请求
        raw_string = self._post_data(url, xml_data)
        # 处理微信返回的xml数据转字典
        return self._xml2dict(raw_string)
