# !/usr/bin/env python
# encoding: utf-8
# @Time    : 2019/1/7 13:54
# @Author  : MengboZh
import logging
import time
import random
from datetime import datetime
from flask import Blueprint, request, jsonify, g, redirect
from app.wechat_oauth2_pay.config import wxpay_config
from app.common import get_random_string
from app.qrcode.utils import CreateQrCode

wechat_pay = Blueprint('wxpay', __name__)


# 付款码支付,test,ok
@wechat_pay.route('micro_pay/', methods=['GET'])
def wxpay_micro_pay():
    # 无扫码枪设备,auth_code(扫码支付授权码，设备读取用户微信中的条码或者二维码信息)暂时写官网例子
    data = wxpay_config.micro_pay(out_trade_no=get_random_string(chars='order', length=32), body='test order',
                                  total_fee=88, auth_code='120061098828009406')
    return jsonify(data)


# JSAPI支付,获取用户授权code, test todo
@wechat_pay.route('jspai_oauth/', methods=['GET'])
def wxpay_jspai_oauth_pay():
    data = {'redirect_uri': 'http://47.92.100.74:5001/wxpay/jspai_pay/', 'state': 'get_code'}
    code_url = wxpay_config.gen_code(data)
    return redirect(code_url)


# JSAPI支付、微信内H5调起支付, test todo
@wechat_pay.route('jspai_pay/', methods=['GET'])
def wxpay_jspai_pay():
    code = request.args.get('code')
    # code获取openid
    wx_data = wxpay_config.gen_code2access_token(code)
    data = wxpay_config.unified_order(out_trade_no=get_random_string(chars='order', length=32), body='test order',
                                      total_fee=88, trade_type='JSAPI', openid=wx_data['openid'],
                                      notify_url=wxpay_config.notify_url)
    res = wxpay_config.build_json_data(data)
    return jsonify(res)


# Native(扫码)支付(模式二),test ok
@wechat_pay.route('qrcode_two_pay/', methods=['GET'])
def wxpay_qrcode_two_pay():
    data = wxpay_config.unified_order(out_trade_no=get_random_string(chars='order', length=32), body='test order',
                                      total_fee=88, trade_type='NATIVE', notify_url=wxpay_config.notify_url)
    res = wxpay_config.build_json_data(data)
    if res.get("code_url"):
        # 根据微信返回的二维码串值,生成二维码图片流
        make_qr_code = CreateQrCode()
        qr_code_file = make_qr_code.gen_qrcode(data=res['code_url'])
        return qr_code_file
    return jsonify(res)


# App支付,test ok
@wechat_pay.route('app_pay/', methods=['GET'])
def wxpay_app_pay():
    data = wxpay_config.unified_order(out_trade_no=get_random_string(chars='order', length=32), body='test order',
                                      total_fee=88, trade_type='APP', notify_url=wxpay_config.notify_url)
    res = wxpay_config.build_json_data(data)
    return jsonify(res)


# H5支付,test ok
@wechat_pay.route('mweb_pay/', methods=['GET'])
def wxpay_mweb_pay():
    data = wxpay_config.unified_order(out_trade_no=get_random_string(chars='order', length=32), body='test order',
                                      total_fee=88, trade_type='MWEB', notify_url=wxpay_config.notify_url)
    res = wxpay_config.build_json_data(data)
    if res.get('mweb_url'):
        return redirect(res['mweb_url'])
    return jsonify(res)


# 小程序支付,test,todo
@wechat_pay.route('caniuse_pay/', methods=['GET'])
def wxpay_caniuser_pay():
    return


# 查询订单
@wechat_pay.route('query_order/', methods=['POST'])
def wxpay_query_order():
    data = wxpay_config.query_order(transaction_id='1009660380201506130728806387', out_trade_no='')
    return jsonify(data)


# 关闭订单
@wechat_pay.route('close_order/', methods=['POST'])
def wxpay_close_order():
    data = wxpay_config.close_order(out_trade_no='1217752501201407033233368018')
    return jsonify(data)


# 申请退款
@wechat_pay.route('refund_order/', methods=['POST'])
def wxpay_refund_order():
    data = wxpay_config.refund(transaction_id='',
                               out_trade_no='1217752501201407033233368018',
                               out_refund_no='1217752501201407033233368018', total_fee=88, refund_fee=86)
    return jsonify(data)


# 查询退款
@wechat_pay.route('refund_query_order/', methods=['POST'])
def wxpay_refund_query_order():
    data = wxpay_config.refund_query(transaction_id='', out_trade_no='', out_refund_no='',
                                     refund_id='1217752501201407033233368018')
    return jsonify(data)


# 下载对账单
@wechat_pay.route('download_bill_order/', methods=['POST'])
def wxpay_download_bill_order():
    data = wxpay_config.download_bill(bill_date='20140603', bill_type='ALL')
    return jsonify(data)


# 下载资金账单, test todo
@wechat_pay.route('download_report_order/', methods=['POST'])
def wxpay_download_report_order():
    data = wxpay_config.report(bill_date='20140603', account_type='Basic')
    return jsonify(data)


# 异步通知
@wechat_pay.route('notify_action/', methods=['POST'])
def wxpay_notify_action():
    # 收到微信支付结果通知后，请严格按照示例返回参数给微信支付
    data = request.form.to_dict()
    if data['return_code'] == 'SUCCESS':
        print('return_msg', data['return_msg'])
        return jsonify("success")

    else:
        print('return_msg', data['return_msg'])
        return jsonify("failed")
