# !/usr/bin/env python
# encoding: utf-8
# @Time    : 2019/1/12 11:38    
# @Author  : MengboZh
import os
from flask_config import ROOR_BASE_DIR
from app.wechat_oauth2_pay.utils import WeChatPay

CERT_PATH = os.path.join(ROOR_BASE_DIR, 'app/wechat_oauth2_pay/cert/')
# sign_type="HMAC-SHA256"
wxpay_config = WeChatPay(appid="wxdac9b58413bcecf6", mch_id="1415371902", key="6F7AEF7D9A4302A3C1E103651356C3D4",
                         sslcert_path=os.path.join(CERT_PATH + 'apiclient_cert.pem'),
                         sslcert_key_path=os.path.join(CERT_PATH + 'apiclient_key.pem'),
                         sandbox=True)

wxpay_gateway = wxpay_config._gateway


def notify_url():
    pass
