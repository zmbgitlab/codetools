# !/usr/bin/env python
# encoding: utf-8
# @Time    : 2019/1/14 17:00    
# @Author  : MengboZh
import logging
import time
import random
from datetime import datetime
from flask import Blueprint, request, jsonify, g, redirect
from app.wechat_oauth2_pay.config import wxpay_config
from app.common import get_random_string
from app.qrcode.utils import CreateQrCode

wechat_oauth2 = Blueprint('wxoauth2', __name__)


@wechat_oauth2.route('', methods=['GET'])
def hello_world():
    return jsonify('hello, world')


@wechat_oauth2.route('oauth2_app/', methods=['GET'])
def wechat_oauth2_app():
    """公众号和移动应用授权登录"""
    pass


@wechat_oauth2.route('oauth2_pc/', methods=['GET'])
def wechat_oauth2_pc():
    """网站应用扫码授权登录"""
    openid = request.cookies.get('openid')
    print('openid:', openid)
    userinfo = None
    if not openid:
        ''''通过code进行获取access_token的时候需要用到，code的超时时间为10分钟，一个code只能成功换取一次access_token即失效'''
        code = request.args.get('code')
        if not code:
            current = "http://" + request.get_host() + request.get_full_path()
            print(u'current方法路径:', current)
            return redirect(WeixinHelper.getqrconnect_login(current))
        else:
            '''通过code进行获取access_token'''
            data = json.loads(WeixinHelper.getAccessTokenByCode(code))
            access_token, openid, refresh_token, expires_in = data['access_token'], data['openid'], data[
                'refresh_token'], data['expires_in']
            print(u'access_token接口调用凭证超时时间,默认7200s:', expires_in)
            request.session['openid'] = openid
            if expires_in > 7200:
                '''使用getAccessTokenByCode()返回的refresh_token刷新access_token'''
                refreshdata = json.loads(WeixinHelper.refreshAccessToken(refresh_token))
                access_token, openid, refresh_token, expires_in = refreshdata['access_token'], refreshdata['openid'], \
                                                                  refreshdata['refresh_token'], refreshdata[
                                                                      'expires_in']
                request.session['openid'] = openid
                '''拉取用户信息,通过网页授权,(需scope为snsapi_userinfo)'''
                userinfo = json.loads(WeixinHelper.getSnsapiUserInfo(access_token, openid))
                print(u'超出7200的拉取用户的信息', userinfo)
            else:
                userinfo = json.loads(WeixinHelper.getSnsapiUserInfo(access_token, openid))
            print(u'未超出7200的拉取用户的信息', userinfo)
        return redirect('/hello')
    else:
        u'已授权过'
        return redirect('/hello')
