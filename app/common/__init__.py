# !/usr/bin/env python
# encoding: utf-8
# @Time    : 2019/1/12 11:16    
# @Author  : MengboZh
import random


def get_random_string(chars='', length=12):
    '''生成12位的随机数字字符串
    :param length: 长度
    :param chars: 可变参数
    :return: 返回随机字符串,默认长度为12，由自定义字符串参数和包含71位的 a-z,A-Z,0-9字符集值。算法:log_2 ((26+26+10)^12) = ~71位
    '''
    """
    Returns a securely generated random string.
    The default length of 12 with the a-z, A-Z, 0-9 character set returns
    a 71-bit value. log_2((26+26+10)^12) =~ 71 bits
    """
    secure_chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    # 使用字符串函数join拼接,该方法多用于列表，元组，对较大的字符串拼接操作
    allowed_chars = ''.join((secure_chars, chars))
    return ''.join(random.choice(allowed_chars) for i in range(length))
