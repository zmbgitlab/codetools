#!/usr/bin/env bash

if [ "$MIGRATE" = "1" ]; then
    python manage.py db init
    python manage.py db migrate
    python manage.py db upgrade

fi

if [ "$MIGRATE" = "2" ]; then
    python manage.py recreate_db
fi

if [ "$MIGRATE" = "0" ]; then
    not migrate
fi

python3 manage.py del_version

# 默认的sync 同步的
#gunicorn -b 0.0.0.0:8080 -w 4 startup:global_app -t 60 --log-level debug
python3 manage.py runserver