###python代码编写的各种工具方法###

[生成二维码]：已完结,生成简单的二维码图片流和带Logo的二维码图片流

[微信支付]：已完结,【付款码支付,JSAPI支付,Native支付(扫码支付-模式二),APP支付,H5支付,小程序支付】
# wxpay开发使用流程

[支付宝支付_新版本]：支付宝新版支付【APP支付,电脑网站支付,手机网站支付等】 已完结

# 支付宝开放平台链接:[https://auth.alipay.com/login/ant_sso_index.htm?goto=https%3A%2F%2Fopenhome.alipay.com%2Fplatform%2FappDaily.htm%3Ftab%3Dinfo]

##*#*#*#*#**#*##*#*#*【沙箱应用】#*#**#*#*#*#**#*#**#:

    APPID：2016080300159687
    蚂蚁沙箱支付宝网关：https://openapi.alipaydev.com/gateway.do
    沙箱平台商家测试账号
    商家账号：pheyvm5471@sandbox.com
    商户UID：2088102169806463
    登录密码：111111
    
    沙箱平台买家测试账号
    买家账号：liwtkv8617@sandbox.com
    登录密码：111111
    支付密码：111111
    用户名称：沙箱环境
    证件类型：身份证(IDENTITY_CARD)
    证件号码：016863194909068950
    开发者可使用商家账号进行应用授权、使用买家账号进行付款等操作；


# alipay开发使用流程:
    1. pip install python-alipay-sdk
    2. 参考 /app/alipay_new/config.py 配置 alipay_config = AliPay()，详情参考config.py
    3. alipay_new/view.py里包含了常用的支付接口,例如: alipay_app_pay方法, 开发者把签名后的订单信息的几行代码，copy到你的方法里就Ok了,
    其他支付也是如此
    ps: alipay_new/cert/ 文件下的三个".pem"文件是生成的密钥文件, alipay_pulic_key.pem(支付宝公钥)、app_private_key.pem(应用私钥)、
    app_public_key.pem(应用公钥),这三个密钥都是在支付宝开放平台按照RSA或RSA2生成的，服务端保留这三个，支付宝开放平台保留 支付宝公钥、应用公钥,
    return_url: 支付宝同步通知url,请求类型(get),是在page_pay、app_pay、wap_pay等支付成功后的支付宝界面，要跳转到哪里，
    推荐是跳转到网站或App首页(为了测试，写的是百度首页)
    notify_url: 支付宝异步通知url,请求类型(post), 是在page_pay、app_pay、wap_pay等支付成功后,支付宝异步请求此接口，发送支付成功后的订单信息，
    服务端依据状态，进行实际的业务开发,notify_url也可在支付宝开放平台设置(应用网关)
[微信授权登录]：

[QQ授权登录]：

[cpp_test上传数据]：已完结,此模块为了测试docker启动的pgsql是否能增删改查数据

