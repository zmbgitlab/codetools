# !/usr/bin/env python
# encoding: utf-8
# @Time    : 2019/1/7 13:54
# @Author  : MengboZh
import os
from flask import make_response, jsonify
from flask_cors import CORS
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, Shell, Server
from app import create_app, db

app = create_app(os.getenv('FLASK_ENV', 'dev'))
# 允许跨域请求
CORS(app, supports_credentials=True)
manager = Manager(app)
migrate = Migrate(app, db)

manager.add_command('shell', Shell())
manager.add_command('db', MigrateCommand)
manager.add_command("runserver", Server(use_debugger=True, port=5050, host="0.0.0.0"))


@manager.command
def create_db():
    db.create_all()


@manager.command
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@manager.command
def load_data():
    pass


@manager.command
def del_version():
    from sqlalchemy import text
    db_session = db.create_scoped_session(options={'autocommit': True, 'autoflush': False})
    sql = text('DELETE FROM alembic_version')
    db_session.execute(sql)
    db_session.flush()


if __name__ == '__main__':
    manager.run()
