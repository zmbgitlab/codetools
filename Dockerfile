###【基础镜像文件】FROM <镜像名>:<镜像标签>,若镜像为latest版本则tag可以省略,否则必须指定版本号,如:FROM python:2.7
FROM python:3.5

###【维护者信息】
MAINTAINER zmb7032@163.com

###【镜像操作指令】
WORKDIR /app

# 将复制宿主机的路径下的文件到容器的目标路径下,宿主机的路径可以为相对路径,容器的目标路径必须为绝对路径,
# COPY指令没有自动解压的功能
COPY . /app

# 复制服务的环境依赖包文件
COPY requirements.txt requirements.txt

###【RUN:执行命令的指令】
# 执行依赖包命令
RUN pip3 install -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple

# 日期修改
RUN cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

# 声明运行时容器暴露的端口等于程序运行的端口号
EXPOSE 5050

###【容器启动时执行指令】
# 打开cmd窗口,执行docker_entry里的命令
CMD ["/bin/bash","docker_entry.sh"]