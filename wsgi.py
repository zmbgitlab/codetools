# !/usr/bin/env python
# encoding: utf-8
# @Time    : 2019/1/7 13:54
# @Author  : MengboZh
from manage import app as instance
from werkzeug.contrib.fixers import ProxyFix

app = ProxyFix(instance)
