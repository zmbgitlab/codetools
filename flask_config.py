# !/usr/bin/env python
# encoding: utf-8
# @Time    : 2019/1/7 13:54
# @Author  : MengboZh
import os
import time

secret_key = '12312'
# 项目根目录
ROOR_BASE_DIR = os.path.abspath(os.path.dirname(__file__))
# 创建项目的路径
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# 项目根目录下的static_file文件目录
EXCEL_PATH = os.path.join(ROOR_BASE_DIR, 'static_file')


class Config:
    JSON_AS_ASCII = False

    # SqlAlchemy
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_POOL_SIZE = 5
    SQLALCHEMY_POOL_TIMEOUT = 10

    with open(os.path.join(ROOR_BASE_DIR, "sql_config.txt"), 'r') as f:
        import json
        temp = json.loads(f.read())
        user = os.environ.get('DB_USER') or temp['config']['DB_USER']
        pwd = os.environ.get('DB_PASSWORD') or temp['config']['DB_PASSWORD']
        host = os.environ.get('DB_HOST') or temp['config']['DB_HOST']
        port = int(os.environ.get('DB_PORT', 0)) or temp['config']['DB_PORT']
        db = os.environ.get('DB_NAME') or temp['config']['DB_NAME']
    data = dict(user=user, pwd=pwd, host=host, port=port, db=db)

    # '''sqlserver 驱动连接'''
    # con_str = 'mssql+pymssql://{user}:{pwd}@{host}:{port}/{db}'
    # SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or con_str.format(**data)
    '''pgsql 驱动连接'''
    con_str = 'postgresql+psycopg2://{user}:{pwd}@{host}:{port}/{db}?client_encoding=utf8'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or con_str.format(**data)
    '''默认sqlite'''
    # SQLALCHEMY_DATABASE_URI = 'sqlite:////default.db'

    # 日志配置
    syslog_tag = "hs_auth"
    LOGCONFIG = {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {},
        'formatters': {
            "defualt": {
                "format": "[%(asctime)s] %(levelname)s - %(message)s"
            },
            "simple": {
                "format": "[%(asctime)s] %(levelname)s - [FileName:%(filename)s] - [FuncName:%(funcName)s] - [LineNo:%(lineno)s} - %(message)s"
            }
        },
        'handlers': {
            'console': {
                '()': 'logging.StreamHandler',
                'formatter': 'defualt'
            },
            'file': {
                '()': 'logging.handlers.RotatingFileHandler',
                'formatter': 'simple',
                'filename': '{}/log_{}.txt'.format(EXCEL_PATH + '/log', time.strftime("%Y-%m-%d")),
                'mode': 'a',
                'maxBytes': 50000,  # 5 MB
                'backupCount': 1,
                "encoding": "utf8"
            },
        },
        'loggers': {
            'myapp': {
                'handlers': ['file'],
                'level': 'DEBUG'
            },
        },
        'root': {
            'handlers': ['console'],
            'level': 'DEBUG'
        }
    }

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    ASSETS_DEBUG = True


class TestingConfig(Config):
    TESTING = True

    @classmethod
    def init_app(cls, app):
        Config.init_app(app)


config = {
    'dev': DevelopmentConfig,
    'tst': TestingConfig
}
